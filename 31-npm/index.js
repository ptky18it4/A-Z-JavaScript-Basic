/* 
    npm - node package manager : cũng giống như tuy duy chia để  trị
        1. readline-sync : tóm lại là sẽ có sự tương tác trên terminal
    quy tac dat ten : cameCase      
*/
var readlineSync = require('readline-sync');
//source
var userName = readlineSync.question('May I have your name? ');
console.log('Hi ' + userName + '!');
//customize
var languages = [];
var language = readlineSync.question('What is your mother language ? ');
languages.push(language);

console.log(languages);