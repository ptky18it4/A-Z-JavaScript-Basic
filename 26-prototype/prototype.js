// //Prototype
// //Create new project
// var mouse = {
//     weight: 0.2,
//     getweight: function() {
//         return this.weight;
//     }
// };
// // contructor function
// function Mouse(color, weight) {
//     this.type = 'mouse';
//     this.color = color;
//     this.weight = weight;
// }

// // var mickey = new Mouse('white', 20);
// // console.log(mickey.color);
// // console.log(mickey.name);

// //Prototype
// console.log(Mouse.prototype.constructor === Mouse);

// /* ==> constructor === Mouse() ---> it's amazing.
//     use : prototype object is shares among all the objects created using new.
//     vi  : là một object được chia sẻ giữa tất các các object mà dùng new
//     --> có nghĩa là tất các object và được tạo ra bởi từ khóa 'new' thì có thể  truy cập vào 'prototype' đó.

//  */

// Mouse.prototype.sleep = function() {
//     console.log('Sleeping .... Zzzzzzz');
// };

// var jerry = new Mouse('Red', 20);
// // console.log(jerry);
// jerry.sleep();

// var mickey = new Mouse('Black', 50);
// // console.log(jerry);
// jerry.sleep();

/* Rồi OK, thế nhưng tại sao không viết như thế này nhỉ */

var mouse = {
    weight: 0.2,
    getweight: function() {
        return this.weight;
    }
};

function Mouse(color, weight) {
    this.type = 'mouse';
    this.color = color;
    this.weight = weight;

    this.sleep = function() {
        console.log("Sleep...Zzzzzzz");
    };
    // Cái này không được chia sẻ cho các object được tạo bằng từ khóa new
}

Mouse.prototype.sleep1 = function() {
    console.log('Sleep....Zzzzzz');
}

var mickey = new Mouse("red", 20);
mickey.sleep();

var jerry = new Mouse('black', 50);
mickey.sleep();

console.log("Không dùng prototype và cái kết : ", jerry.sleep === mickey.sleep);

console.log("Dùng prototype và cái kết : ", jerry.sleep1 === mickey.sleep1);

/**
 * CHÚ Ý : 
 * Đối với việc dùng prototype thì sẽ tối ưu hóa bộ nhớ hơn.
 * Thực tế  thì người các developer cũng dùng cái này --> chuẩn .
 * Trình độ pro hơn.
 * Gái theo nhiều hơn :)) "đùa thôi nhé ! kkk
 */