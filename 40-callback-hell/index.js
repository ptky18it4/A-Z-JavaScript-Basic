/**
 * Hey, xin chào.
 * Hôm nay sẽ chém gió về chủ đề "Callback hell" trong lập trình  javascript
 *
 * */
var fs = require('fs');
var song1 = fs.readFileSync('song1.txt', { encoding: 'utf8' });
var song2 = fs.readFileSync('song2.txt', { encoding: 'utf8' });
var song3 = fs.readFileSync('song3.txt', { encoding: 'utf8' });
var song4 = fs.readFileSync('song4.txt', { encoding: 'utf8' });
console.log(song1, song2, song3, song4);

/**
 * Rồi OK, bây giờ tiếp tục đến với khái niệm thần thánh callback hell
 * Mời bạn chiêm ngưỡng kim tự tháp thời cổ đại
 */
fs.readFile('./song1.txt', { encoding: 'utf8' }, function(err, song1) {
    console.log(song1);
    fs.readFile('./song2.txt', { encoding: 'utf8' }, function(err, song2) {
        console.log(song2);
        fs.readFile('./song3.txt', { encoding: 'utf8' }, function(err, song3) {
            console.log(song3);
            fs.readFile('./song4.txt', { encoding: 'utf8' }, function(err, song4) {
                console.log(song4);
            });
        });
    });
});
/**
 * Code thể này nhìn khoai vl, làm cách nào để optimize nó đây nhỉ ?
 * ???
 * ???
 * ???
 * ??
 * ??
 * ?
 * ?
 * Bài sau sẽ có nhé !
 */