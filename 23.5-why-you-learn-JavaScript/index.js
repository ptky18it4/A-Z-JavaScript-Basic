/**
 * What will you learn ?
 * Front-end
 * 1. JavaScript basic
 * 2. JavaScript advanced
 * 3. CSS
 * 4. HTML
 * 5. Boostrap
 * -> Some front-end projects to practice
 * 6. NodeJS
 * 7. ReactJS
 * -> Some UI projects with React
 * 
 * --------------------------------------------
 * Back-end
 * 8. ExpressJS
 * 9. Git, Trello, Asana
 * 10. Deployment tools
 * -> Then you will work on some projects.
 * 11. Interview skills
 * 12. How to work in a team efficiently
 * 13. Others: ReactNative, PhoneGap, Electron, AWS, Sketch, Ruby ( ngôn ngữ mà người nhật khá là thích )
 * 14.
 * 15.
 * 16.
 */
/**
 * Hầu hết mọi người khi bắt đầu học lập trình web thì đâu đó vẫn còn một số người khuyên là: ừ cậu nên học php đi, à, giờ m muốn 
 * học làm web thì cứ học chắc php cho tao, bla bla,......và cứ thế nghe lời người khác mà cắm đầu học php.
 * Bạn nên nhớ rằng, những gì người khác nói không hẳng là đúng, ở đây mình không nói họ khuyên sai nhưng trước khi học một cái gì
 * đó, bạn cần nắm rõ các yếu tố cơ bản sau:
 * a. Mục đích của bạn muốn nhắm tới là gì ?
 * b. Công cụ hỗ trợ bạn đạt được mục đích đó là gì ? ở đây là ngôn ngữ lập trình 
 * c. Ngôn ngữ này hiện nay trên thị trường có nhiều người làm hay không ?
 * d. Ngôn ngữ này làm được những gì ( ưu điểm ? nhược điểm ) so với ngôn ngữ PHP. Hiện nay những thứ PHP làm được thì JavaScript cũng 
 *    làm được, có khi còn làm tốt hơn cả PHP
 * e. Nhu cầu thị trường thế nào ? Điều quan trọng là mức lương mà bạn nhận được khi bạn biết  JavaScript với biết PHP như thế nào?
 * f. Nếu đã quyết định làm thì phải xác định rõ : Mục tiêu - Kiên trì - Thời gian -Sức khỏe
 *           f.1 : Mục tiêu: làm việc phải có mục tiêu ( nói vui: phải biết nhà con bồ mày ở đầu thì mới có cơ hội thả thính mỗi ngày chứ).
 *           f.2 : Kiên trì: hãy kiên trì đến cùng, hãy cháy hết mình với con đường mình đã chọn để , Kết quả <---> Nỗ lực.
 *           f.3 : Thời gian: cố gắng học càng nhanh càng tốt, làm càng nhiều càng tốt, va chạm càng nhiều càng tốt, đọc tài liệu,
 *                            nghiên cứu về nó càng nhiều càng tốt.
 *           f.4 : Sức khỏe : hãy giữ gìn sức khỏe của mình, đừng làm phiền đến mọi người xung quanh.
 * g. Học JavaScript không chỉ làm được web, mà nó còn làm được cả app và một số thức khác cho nên bạn không cần lo sợ về việc không
 *    có việc làm về sau ( học 1 làm được 10)
 * h.Hãy chọn cho mình một con đường mới: thầy dạy mình 1 ngôn ngữ chứ không dạy mình kiếm tiền từ ngôn ngữ đấy như thế nào ? cho nên
 *   hãy học để đáp ứng nhu cầu của thầy và đừng quên, hãy tự OT ( overtime ) ở nhà để cày thêm cho mình một ngôn ngữ mà hiện nay thị
 *   trường đang cần ( điển hình là JavaScript nhé !).
 * i. Thường xuyên tham gia các câu lạc bộ/ xem các bài báo về công nghệ/ thanh gia các group,.... để bắt kịp xu hướng của thế giới
 * j. Đừng nghĩ đến việc học xong, ra trường làm ở các công ty Việt Nam, hãy nghĩ lớn hơn, ra trường phải làm được ở các công ty nước
 *    ngoài, bởi vì nghề lập trình viên bạn không bao giờ sợ thất nghiệp, vì đó là nghề của cả thế giới, có thể làm ở bất kì đầu.
 */