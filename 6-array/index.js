/**
 * OK , các bạn nhớ dành ra 5-10 phút để xem lại bài cũ để hệ thống lại những gì đã học nhé!
 * Bài hôm nay sẽ nói về  array trong JavaScript.
 * Về khái niệm array : nó là một cái mảng để lưu các giá trị mà bạn khai báo trong quá trình lập trình
 * Nguồn tài liệu : https://javascript.info/array
 * -----------------------------------------------------------------------------------------------------
 * Khai báo 3 biến:
 * var a = 1;
 * var b = 2;
 * var c = 3;
 * Và các giá trị được khai báo ở trên được sắp xếp ngẫu nhiên vào mảng phía dưới:
 * Và để  khai báo một mảng thì ta dùng cú pháp :
 * Systax : var nameArray = [element_1, element_2, element_3,..... ];
 * 
 *                index:-->     0          1         2            3
 * 
 *                          |--------|----------|-----------|-----------|-----------|
 *             array        |   1    |     2    |           |     3     |   ....    |
 *                          |--------|----------|-----------|-----------|-----------|
 * 
 *                length:--><--------------------4---------------------->
 *  
 * 
 * Thuật ngữ : term - trong mảng :
 * - array : mảng
 * - element : các phần tử
 * - index : các chỉ số trong mảng
 * - length: chiều dài của mảng
 */
var user_0 = {
    name: 'kenneth',
    age: 19

};
var user_1 = {
    name: 'trungky',
    age: 20

};
var user_2 = {
    name: 'peter',
    age: 18

};

var users = [user_0, user_1, user_2];
console.log('Xem tất cả users : \n', users);
console.log('Xem  user 2 tên gì : \n', users[2].name);
console.log('\n-----------------------------------------------');

/**
 * Thay user 2 thành user mới
 * 
 */
user_4 = {
    name: 'Cà khịa',
    age: 1
};
users[0] = user_4;
console.log('Xem tất cả users : \n', users);
console.log('Xem  user 0 tên gì : \n', users[0].name);
console.log('\n-----------------------------------------------');
/**
 * Trong thực tế : array dùng để  lưu trữ 1 list ( danh sách ) gồm các phần tử giống nhau.
 * VD : danh bạn điện thoại
 */
var contact_0 = {
    name: 'Daddy',
    age: 0,
    phone: '1234567890'
}
var contact_1 = {
    name: 'Daddy',
    age: 0,
    phone: '0987654321'
}
var contact_2 = {
    name: 'Daddy',
    age: 0,
    phone: '54321567890'
}
var contacts = [contact_0, contact_1, contact_2];
console.log(contacts);
console.log('\n-----------------------------------------------');

/**|||||||||||||||||||||||||||||||||||||||||||||- RỒI OK - CHÚNG TA KẾT THÚC BÀI HỌC Ở ĐÂY NHÉ ! -|||||||||||||||||||||||||||||||| */
S