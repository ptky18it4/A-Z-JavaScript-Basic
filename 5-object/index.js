/**
 * Hôm nay sẽ học về kiểu OBJECT và ARRAY
 * -OBJECT : vậy object là gì ? object là một kiểu dữ liệu rất hay dùng trong ngôn ngữ lập trình JavaScript
 * -Use : dùng để mô tả một đối tượng nào đấy ? ( con người ; chiều cao, cân năng, màu tóc, àu da, ...)
 * -Syntax : 
 *          var nameObject = {
 *               key : value
 *          };
 * 
 * - Quy tắc đặt tên Object : camelCase
 */
var a = {
    /**
     * Các thuộc tính
     * Mỗi thuộc tính được khai báo bởi một cặp gọi là : key : value
     * Chú ý: syntax: là lỗi mà các bạn hay gặp phải, hay sai về dấu (, ; v.v).
     * Và nếu là chuỗi ( string ) : đặt giá trị trong dấu ''.
     * Đối với số ( number ) : thì không cần
     */
    name: 'Kenneth',
    age: 19
};

/** 
 * Vậy thì làm sao để  lấy được tất cả các giá trị trong object a / chỉ  name or age từ objec a ở trên nhỉ ?
 * Syntax : nameObject.key
 */
var account = {
    username: 'admin',
    password: 'passAdmin',
    enable: true

};
console.log('Load dữ liệu từ object : \n - Syntax: nameObject.key \n');
console.log('Load cả object', account);
console.log('Load username : ', account.username);
console.log('Load password : ', account.password);
console.log('\n------------------------------------------------------\n');
/**
 * Vậy thì làm sao để thay đổi thuộc tính password nhỉ
 * -Chỉ cần gọi password ra và gán giá trị mới cho nó là xong.
 * -Xem ví dụ để hiểu nhé !
 */

account.password = 'newPass'; // sau đó console.log(account) thì  --> output: { username: 'admin', password: 'newPass' }
console.log(account);
console.log('Load password : ', account.password);
console.log('\n------------------------------------------------------\n');
/**
 * OK, vậy thì còn cách nào để  lấy dữ liệu ra bằng cách sử dụng dấu chấm hay không nhỉ ?
 * Bây giời mình sẽ giới thiệu đến bạn một cách nữa cũng khá là hay đó là ......xem code ở phía dưới nhé!
 * Tiếp tục sử dụng object account ở phía trên nhé 
 * Use : dùng đấu ngoặc nameObject[' key ']
 */
account['enable'] = false; // chú ý là phải có dấu nháy nha. k chú ý là mất tiền như chơi đó :))
console.log(account);
console.log(account['password']);
console.log('\n------------------------------------------------------\n');


/**
 * RỒI OK, TÚM LẠI LÀ OBJECT DÙNG ĐỂ MIÊU TẢ MỘT ĐỐI TƯỢNG CÓ NHIỀU THUỘC TÍNH KHÁC NHAU. HẸN GẶP LẠI BẠN Ở BÀI SAU
 */