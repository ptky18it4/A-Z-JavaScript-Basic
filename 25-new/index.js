/**
 * Hey you, welcome to with course A-Z JavaScript basic
 * You should complete homework before learn.
 * Keyword : new 
 * I think it really importan in Code
 */
var today = new Date();
console.log(today);
/**
 * The way create new object in JavaScript
 */
var mouse = {
        name: 'Jerry',
        age: 2,
        weight: 2,
        hobbi: 'eat',
        address: 'USA',
        getAddress: function() {
            return this.address;
        }
    }
    /**
     * Tại sao từ Date() lại viết hoa nhỉ ?
     * Giải thích : trọng JavaScript có một constructor function 
     */
console.log(mouse.getAddress()); /**Bạn nào không nhớ thì xem lại bài object nhé */

function Mouse(color) {
    this.type = 'mouser';
    this.color = color;
}
var mouse_1 = new Mouse();
var mouse_2 = { type: 'mouse' };
var mouse_3 = new Mouse('while');
console.log(mouse_1); //-> load ra kiểu của object và object
console.log('\n----------------------------------------------------------');
console.log(mouse_2); //-> load ra : chỉ load ra object
console.log('\n----------------------------------------------------------');
console.log(mouse_3);
console.log('\n----------------------------------------------------------');
/**Tấc cả các function() trong JavaScript đều có thể gọi new được */
/**
 * Thằng tom có stomach (rỗng), sau khi cho tom eat thì push(mouse) vào stomach
 */
var tom = {
    name: 'Tom',
    age: 19,
    stomach: [],
    eat: function(mouse) {
        this.stomach.push(mouse);
        return this;
        console.log(this);
    }
};
var m0 = { name: 'Tom' };
var m1 = { name: 'Teo' };
var m2 = { name: 'Tun' };
console.log(tom.eat(m0).eat(m1).eat(m2));