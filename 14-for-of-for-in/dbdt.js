var employees = [
    { name: 'Kenneth', age: 11 },
    { name: 'peter', age: 12 },
    { name: 'Jack', age: 14 },
    { name: 'Nhu', age: 19 },
    { name: 'Thinh', age: 19 },
    { name: 'Nam', age: 19 },
    { name: 'Thinh', age: 19 }
];

var content = '';
for (var employee of employees) {
    content += '<li>' + employee.name + '</li>';
    console.log(employee.name, employee.age);
    /*content += '<li>Kenneth</li>'
      content += '<li>peter</li>'
      content += '<li>Nhu</li>'
      -> content = <li>Kenneth</li><li>peter</li><li>Nhu</li>
      Ta thấy ở đây có dấu công: thì dấu cộng ở đây khi sử dụng
      với mảng thì nó có tác dụng là công chuỗi
    */
}

document.getElementById('contact-list').innerHTML = content;