/**
 * Tiếp nối bài : FOR LOOP
 * Hôm nay chúng ta sẽ học về : 
 *  -----------------------------------------------------------------------------------------------------------------------------
 *                                               --FOR LOOP / VÒNG LẶP FOR--
 * ----------------------------------------------------------------------------------------------------------------------------- 
 *                                               --| for-of, fo-in, ....|--
 */
/**------------------------------------------------------FOR - OF--------------------------------------------------------------- */

var employees = [
    { name: 'Kenneth', age: 11 },
    { name: 'peter', age: 12 },
    { name: 'Jack', age: 14 },
    { name: 'Nhu', age: 19 },
    { name: 'Thinh', age: 19 },
    { name: 'Nam', age: 19 },
    { name: 'Thinh', age: 19 }
];
for (var employee of employees) {
    console.log(employee.name, employee.age);
}
/**
 * Biến employee thường đặt : bạn có thể đặt tên gì cũng được nhưng thông thường thì đặt là số ít ( cùng với tên mảng để  dễ hiểu)
 * Ưu điểm : viết dễ hơn vòng lặp cổ điển 
 * Nhược điểm: không biết được nó lặp qua phần thử nào ( vì không có chỉ số)
 */
console.log('\n------------------------END FOR-OF------------------');

// Ứng thực tế : danh bạ điện thoại
/**------------------------------------------------------FOR - IN---------------------------------------------------------------\
 * Ok , điểm khác biệt: lặp qua các key của các element của mảng, nhớ là của mảng nha, chứ k phải object đâu
 */
var employees_2 = {
    name: 'Kenneth',
    age: 11,
    class: '18it4',
    hometown: 'Phu Yen'
}
for (var key in employees_2) {
    console.log(key, employees_2[key]);
    /**
     * Muốn lấy value thì làm thế nào ?
     * Để truy cập vào key của 1 object thì có 1 cách
     * 1 : dùng dấu chấm (.) / employees_2.name = Kenneth
     * 2 : dùng dấu ngoặc vuông [ 'value' ] / employees_2[ 'name' ] = Kenneth
     * Thì đối với vòng lặp for-in này cũng vậy, cũng áp dụng tương tự.
     * Chú ý: ta cần truyền tham số vào dạng như sau :  nameArray(key, nameArray[ key ])
     * 
     * Rồi OK, cảm ơn bạn đã xem đến cuối bài giảng
     */
}