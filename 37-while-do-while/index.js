/**
 * while
 * 
 * Syntax:
    while (condition) {
        do something
    }
 * 
 */
var i = 0;
while (i < 10) {
    console.log(i);
    i++;
}

/**
 *  do.. while
 * 
 * Syntax:
 
    do {
        
    } while (condition);
 * 
 */
console.log('\n')
var j = 0;
do {
    console.log(j);
    ++j;
} while (j <= 10);