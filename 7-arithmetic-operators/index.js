/**
 * Bạn hãy dành ra 5- 10 phút để ôn tập lại kiến thức ở bài trước để hệ thống lại những kiến thức đã học nghen.
 *
 * Arithmetic operator : các phép toán số học logic trong lập trình
 * Các phép toán số học trong lập trình.
 * 1. ++ -- (increment decrement)
 * 2. * / %
 * 3. + -
 * 4.
 */
var a = 8;
var b = 10;
var c = 20;
// Toán tử  : + , -
console.log('Giá trị khi cộng 2 số : ', a + b);
console.log('Giá trị khi trừ 2 số : ', a - b);
console.log('\n-------------------------------------------------------------------');
// Toán tử  : * , / , %
console.log('Giá trị khi nhân 2 số : ', a * b);
console.log('Giá trị khi chia 2 số : ', a / b);
console.log('Giá trị khi chia lấy dư 2 số : ', a % b);
console.log('\n-------------------------------------------------------------------');

console.log('Các số không chia hết cho 2 : ');
for (var i = 0; i < 10; i++) {

    if (i % 2 != 0) {
        console.log(i);
    }
}
/**CHÚ Ý: THỨ TỰ THỰC HIỆU : TỪ TRÁI SANG PHẢI */
// Toán tử  : ++ , -- sẽ nhắc ở bài sau