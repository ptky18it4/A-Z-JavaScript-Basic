/**
 * Bạn nhớ dành thời gian tầm 5 10 phút để  cover lại kiến thức + bài tập ngày hôm trước nhé !
 * 
 * Rồi OK : Chào mừng bạn quay lại với khóa học A-Z JavaScript Basic nhé!
 * Bài hồm nay, sẽ nói về :                       -- FUNCTION / HÀM --
 *                                                  
 * Khái niệm: Function(), được hiểu đơn giản, nó có nhiệm vụ là thực hiện một chức năng, một logic, hoặc là 1 nhiệm vào nào đấy
 *            VD: như 1 cái máy pha cafe thì :
 *
 *                  nguyên liệu-->|        ||||||||||||||||                      | cafe 
 *                             -->|------->| máy pha cafe |---dữ liệu đầu ra---> |
 *                  input data -->|        ||||||||||||||||                      | giá trị đích mà người dùng cần.
 *
 * ------------------------------------------------------------------------------------------------------------------------------
 *                                                 --DEFIND A FUNCTION--
 * ------------------------------------------------------------------------------------------------------------------------------
                        function doSomething(input1, intput2) {
                            //code
                            return doSomething;
                        }


                        key : function,return;
                        Function name: doSomething ./ tên function thông tường là một động từ ( chức năng của nó)
                        Parameter names: input1, input2
 *                      
 * -Khi bạn khai báo 1 function thì nó sẽ được lưu vào bộ nhớ ( giống như 1 biến ), nhưng khác biến ở chố là có thể gọi nó để thực
 *  thi một nhiệm vụ nào đó
 * - Xem ví dụ phía dưới để hiểu hơn về những gì liên quan đến Function
 *      + cách khai báo
 *      + cách gọi lại hàm để nó thực thi ( execute/ Call a function)
 *      + ..../ tiếp súc rồi sẽ biết
 * ------------------------------------------------------------------------------------------------------------
 *                          -----CÁC BƯỚC KHI LÀ VIỆC VỚI FUCNTION------
 * ------------------------------------------------------------------------------------------------------------
 * B1: Defind function
 * B2: Execute/Call fucntion
 **/

// Function: tính diện tích của 1 tam giác ( Công thức : S = a*h/2)
//B1 :
function calculateTryangleSquare(a, h) {
    return a * h / 2;
}
//B2 :
console.log(calculateTryangleSquare(10, 5));
console.log('\n-------------------------------');


// Function: tính diện tích hình tròn ( Công thức : S = r*r*3.14)
//B1 :
function calculateDiscSquare(r) {
    return r * r * 3.14;
}
//B2 :
console.log(calculateDiscSquare(15));
/**
 * OK, cũng dễ chứ không khó đúng không nào.
 * Bạn hãy, nếu như đã hiểu về cách thức hoạt động của function thì còn chần chờ gì nữa. Hãy làm ngay bài tập
 * 1. Tính diện tích hình vuông
 * 2. Tính tiền điện (cố gắng optimize bài toàn này, càng hoàn thiện càng tốt)
 * 3. Tính diện tích, chu vi các hình 
 * CHÀO TẠM BIỆT, HẸN GẶP LẠI BẠN Ở BÀI SAU
 */