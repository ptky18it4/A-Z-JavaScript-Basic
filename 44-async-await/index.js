/**Node >= 7.6 */
/**
 * Ở bài này ! mình sẽ tiếp cận với một module mới khác được đó chính là :
 * 
 *                     ASYNC AWAIT 
 * không biết cao nhân nào phát triển ra cái này nữa. Nhưng mà nó rất là hay,
 * giúp code ngắn gọn và khó hiểu hơn  :))
 *
 */
var fs = require('fs');

function readFilePromise(path) {
    return new Promise(function(resolve, reject) {
        fs.readFile(path, { encoding: 'utf8' }, function(err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

async function run() {
    var song1 = await readFilePromise('./song1.txt');
    var song2 = await readFilePromise('./song2.txt');
    return [song1, song2];
}

run().then(function(values) {
    console.log(values);
});