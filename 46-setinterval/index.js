/**
 * Hàm setInterval này nó sẽ hoạt động như sau:
 * cứ sau một khoảng thời gian milisecond thì nó sẽ
 * gọi lại hàm fn.
 * setInterval(fn,ms);
 * 
 * clearInterval
 * 
 * Hàm phía dưới được hiểu như sau;
 * cứ sau 1 giây thì i sẽ tăng lên 1 và xuất ra mà hình. OK
 * Easy 
 */

var i = 10;
var intervalID = setInterval(function() {
    --i;
    console.log(i);
    if (i === 0) {
        console.log('Boooom !!!');
        clearInterval(intervalID);
    }
}, 1000);