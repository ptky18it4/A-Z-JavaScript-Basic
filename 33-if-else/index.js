/**
 * if ... else  ( in javascript )
 * if (condition) {
        statement when true
    } else {
        statement when false
    }
 * Example 1 : Toss a coin 
 */
/** TUNG ĐỒNG SU */
function tossCoin() {
    // show random values
    var value = Math.random();
    if (value < 0.5) {
        console.log('Mặt sấp');
    } else {
        console.log('Mặt ngửa');
    }
}
// tossCoin();
/**TÁN GÁI */
function shouldIDateHer() {
    // show random values
    var value = Math.random();
    if (value < 0.5) {
        console.log('Yes, of course');
    } else {
        console.log('No, she has a boyfriend');
    }
}
// shouldIDateHer();

/** ĐẾM TIỀN  */
function countBills(bills) {
    /*
    chú ý : giá trị truyền vào ở đây không nhất thiết
    phải là bills.
    Ở đây, vô tình mình đặt tên giống như vậy thôi à !
    - Đối với vài này thì bạn có thể dùng reduce() để tính nhé !
    - Nhưng đang học về  câu điều kiện if..else nên mình sẽ dùng vòng lặp for :)) -> liên quan vcl ( vô cùng lớn :)))
    */
    var total = 0;
    for (var bill of bills) {
        if (!bill.fake) {
            total += bill.value;
        } else {
            console.log('fake bill', bill.value);
            break;
        }
    }
    return total;
}
var bills = [
    { value: 10000 },
    { value: 20000 },
    { value: 50000 },
    { value: 100000, fake: true },
    { value: 200000 },
    { value: 500000 },
];
var total = countBills(bills);
console.log(total);