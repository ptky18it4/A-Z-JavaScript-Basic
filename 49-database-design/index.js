/**
 * Một số skill cơ bản khi thiết kế cơ sở dữ liệu  trong lập trình
 */
var classes = [{
        id: 0,
        name: '1A',
        teacher: 0,
        students: [
            { name: 'Kenneth', height: 120 },
            { name: 'TrungKy', height: 180 }
        ]
    },
    {
        id: 1,
        name: '2A',
        teacher: 1,
        students: [
            { name: 'Kenneth', height: 120 },
            { name: 'TrungKy', height: 180 }
        ]
    },
    {
        id: 2,
        name: '3A',
        teacher: 2,
        students: [
            { name: 'Kenneth', height: 120 },
            { name: 'TrungKy', height: 180 }
        ]
    },
    {
        id: 3,
        name: '4A',
        teacher: 3,
        students: [
            { name: 'Kenneth', height: 120 },
            { name: 'TrungKy', height: 180 }
        ]
    },
    {
        id: 4,
        name: '5A',
        teacher: 4,
        students: [
            { name: 'Kenneth', height: 120, class: 0 },
            { name: 'Peter', height: 120, class: 0 },
            { name: 'TrungKy', height: 180, class: 0 }
        ]
    }
];
var teacher = [{
        id: 0,
        name: 'Kim Anh',
        age: 20
    },
    {
        id: 1,
        name: 'Quynh',
        age: 20
    },
    {
        id: 2,
        name: 'Nam',
        age: 20
    },
    {
        id: 3,
        name: 'Canh',
        age: 20
    },
    {
        id: 4,
        name: 'Anh',
        age: 20
    },
];
var students = [
    { name: 'Kenneth', height: 120, class: 0 },
    { name: 'Peter', height: 120, class: 0 },
    { name: 'TrungKy', height: 180, class: 0 }
];

function getStudentsInClass(className) {
    var classObject = classes.find(function(x) {
        return x.name === className;
    });
    var studentsInClass = students.filter(function(student) {
        return student.class === classObject.id;
    });
    return studentsInClass;
}
var studentsInClass = getStudentsInClass('1A');
console.log(studentsInClass);