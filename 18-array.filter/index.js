/**
 * filter : trong tiếng anh : gọi là màng lọc
 * Hiểu đơn giản đó là : filter nó giống như một cái vợt, những con cá nào bự quá (lớn hơn kích thước lỗ lưới) thì bị mắc kẹt lại
 * còn những con cá nhỏ thì nó có thể thóat ra ngoài
 *-----------------------------------------------------------------------------------------------------------------------------
 *                                         [x1      ,x2      ,x3      ,x4]
 *                                        ________________________________
 *                                       |                                |
 *                                        \                              /
 *                                         \                            /
 *                                          \         Điều kiện        /
 *                                           \                        /
 *                                            \    filter function   /
 *                                             \                    /
 *                                              \                  /
 *                                               \                /
 *                                                \              /
 *                                                 \            /
 *                                                  \          /
 *                                                   \        /
 *                                        ________________________________
 *                                                    |      |
 * 
 *                                                  [x1     ,x4]
 * 
 * Rồi OK, hoàn thành xong khóa này. Trình vẽ của mình sẽ tăng cao
 *  
 */
var numbers = [1, 2, 3, 4];
var evenNumbers = numbers.filter(function(item) {
    return item % 2 === 0;
});
console.log(evenNumbers);