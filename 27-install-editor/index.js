/**
 * Ở bài này mình sẽ giới thiệu đến các bạn những trình soạn thảo mã code ( editor ) mà hiện nay các công ty
 * đang sử dụng.
 * Thì câu hỏi đầu tiên dành cho bạn : 
 *  1. Bạn đang dùng editor nào ?
 *  2. Ai là người chỉ bạn sử dụng editor đó ?
 *  3. Cảm nhận sau khi sử dụng editor đó  ?
 * Đối với ae lập trình viên thì phần mềm mà ae sử dụng nhiều nhất đó chình là editor. Nhưng có một thực trạng
 * mà các ae lập trình viên hay mắc phải đó chính là không chịu thay đôi. Cứ dùng mãi một trình soạn thaor(editor)
 * mà không chịu thay đổi qua thời gian.
 * Ae nên nhớ, cái mình sử dụng, chưa chắc gì sau này ra công ty mà công ty nó sử dụng. Vì hầu hết hiện nay các
 * công ty họ đều đầu tư sử dụng những Editor nổi tiếng, được các lập trình viên chuyên nghiệp đánh giá cao trên
 * Google. 
 * Nhiều khi ae gặp bug, nhờ đồng nghiệp kế bên fix hộ thì lại xảy ra 1 chuyện hy hữu thế này. Bạn của bạn sử dụng
 * VS CODE còn bạn thì sử dụng SUBLIME TEXT thì hỏi thử xem bạn của bạn có giúp đỡ hay không. Hay là họ ngại. Vì
 * nhiều yếu tố sau đây : 1 bên thì code thật đẹp, có các extension hỗ trợ căn chỉnh các kiểu, còn 1 bên thì code
 * như tơ tằm, nhìn muốn lòi con măt, mà lập trình viên thì hầu như thằng nào cũng cận, mà bạn nghĩ sao bạn nỡ làm
 * cho 2 con mắt nó lòi ra luôn à. Như thế gọi là sống : không có đạo đức đó nghen
 * 
 * Ok, chém gió thế đủ rồi :
 * 5 phút giới thiệu các editor:
 * 1. Google keyword: editor code 
 */