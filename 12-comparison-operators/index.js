/**
 * Bạn hãy dành thời gian tầm 5 - 10 phút để ôn lại nội dung bài cũ / hoàn thành Examples của bài hôm trước nhé
 * Rồi OK. Thì hôm nay chúng ta sẽ học bài : 
 * -----------------------------------------------------------------------------------------------------------------------------
 *                                               --COMPARISON OPERATOR / CÁC PHÉP SO SÁNH--
 * ----------------------------------------------------------------------------------------------------------------------------- 
 *                                               --| > , >= , <, <=, ===, !== / ==, != |--
 *
 * Ở bài này mình sẽ không code, mà chỉ gửi đến bạn đường dẫn dể các bạn vào đọc tài liệu thôi! 
 * Nguồn : https://javascript.info/comparison
 * Rồi OK, chào tạm biệt bạn và hẹn gặp lại ở bài sau.
 */