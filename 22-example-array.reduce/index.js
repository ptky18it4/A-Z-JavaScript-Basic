/**
 * Đây là một ứng dụng khá là thực tế, các bạn cố gắng hiểu rõ đề vận dụng nhé !
 * Example : Cart 
 *  requested/ job : calculator for customers
 *  
 */
console.log('\n----------------------------------------------------------');
var products = [
    { name: "TV SAMSUNG 56 INCH", quantity: 6, unitPrice: 55.890 },
    { name: "Laptop Thinkpad P1 carbon xeon 2019", quantity: 2, unitPrice: 44.998 },
    { name: "GoPro DJI 1800", quantity: 1, unitPrice: 12.990 },
    { name: "Casio 1200WHD -  day da", quantity: 2, unitPrice: 1.246 },
    { name: "Flycam DJI Fantom 5", quantity: 3, unitPrice: 35.890 }
];

// Algrothm calculate total of product
// total = ?
var total = products.reduce(function(currentTotal, product) {
    if (product.quantity === 0 || product.unitPrice === 0) {
        console.log("please check from basket");
    } else {
        return currentTotal + product.quantity * product.unitPrice;
    }
}, 0);
/**
 * Chú ý giá trị 0 truyền vào ở ở sau function ở trên.
 * Tham số thứ 2 với giá trị 0 này được thêm vào bài toán:
 * tiêu chí để lựa chọn tham số thứ 2 này là gì ?
 */
console.log("Total of basket is : " + total, 'đ');

/**
    var total = products.reduce()
 * 
 * reduce(callbackfn: (previousValue: { name: string; quantity: number; unitPrice: number; },
 *                     currentValue: { name: string; quantity: number; unitPrice: number; },
 *                     currentIndex: number,
 *                     array: { name: string; quantity: number; unitPrice: number; }[]) 
 *              => { ...; }): { name: string; quantity: number; unitPrice: number; }
A function that accepts up to four arguments.
 The reduce method calls the callbackfn function one time for each element in the array.


Calls the specified callback function for all the elements in an array.
The return value of the callback function is the accumulated result, and is provided as an argument in the next call to the callback function.
 */