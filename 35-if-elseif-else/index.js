/**
     if (condition) {
         statement1;
    } else if (condition) {
        statement2;
    } else {
        statement3;
    }
 * Example calulate bus ticket free
 - if age < 15, discount = 20%
 - if age > 60, discount = 10%
 - otherwise 10000/ticket
 */
function getTicketFree(person) {
    var basePrice = 10000;
    if (person.age < 15) {
        return 10000 * 0.8;
    } else if (person.age > 60) {
        return 10000 * 0.9;
    } else {
        return basePrice;
    }
}
var person = {
    age: 16
}
var fee = getTicketFree(person);
console.log(fee);

/**
 * Đến đây : các bạn có thể  áp dụng các kiến thức ở bài trước
 * về  readLine.question : để nhận giá trị từ bàn phím -> giá .
 */
s