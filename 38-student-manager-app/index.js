/**
 * Requirements :
 * A student management app that is able to:
 * -Show currnet student list
 * -Add new students
 */
var readlineSync = require('readline-sync');
var fs = require('fs');

var students = [];

function loadData() {
    var fileContent = fs.readFileSync('./data.json');
    students = JSON.parse(fileContent);
    // console.log(students);
}

function showMenu() {
    console.log('1. Show all students');
    console.log('2. Create a new students');
    console.log('3. Save & Exit');
    var option = readlineSync.question('> ');
    // console.log(option);
    switch (option) {
        case '1':
            showStudent();
            showMenu();
            break;
        case '2':
            showCreateStudent();
            showMenu();
            console.log(students);
            break;
        case '3':
            saveAndExit();
            break;
        default:
            console.log('Wrong option');
            showMenu();
            break;

    }
}

function showStudent() {
    for (var student of students) {
        console.log(student.name, student.age);
    }
}

function showCreateStudent() {
    var name = readlineSync.question('Name : ');
    var age = readlineSync.question('Age : ');
    var student = {
        name: name,
        age: parseInt(age)
    };
    students.push(student);
}

function saveAndExit() {
    var content = JSON.stringify(students);
    fs.writeFileSync('./data.json', content, { ecoding: 'utf8' });
}

function main() {
    loadData();
    // Kiểm tra xem thử đã đọc được file chưa.
    // console.log(students);
    showMenu();

}
main();