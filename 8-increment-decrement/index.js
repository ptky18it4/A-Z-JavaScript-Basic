/**
 * Bạn hãy dành thời gian ra tầm 5 - 10 phút để ôn lại bài cũ và tiếp tục chuẩn bị cho bài mới nhé!
 * Rồi OK, bài hôm nay chúng ta sẽ học về  2 toán tử rất là quan trọng trong lập trình đó chính là : 
 *  ++ : increment
 *  -- : decrement
 * Để hiểu hơn về 2 toán tử trên thì bạn hãy xem ví dự dưới đây nhé!
 * 
 */

var x = 1;
var y = 2;
/** Rồi OK : ++a có nghĩa là mình tăng a lên trước rồi mới in */
console.log(++x); // output : 2

/** Rồi OK : a++ có nghĩa là mình in a ra trước rồi mới tăng lên */
console.log(x++); // output : 1

/** Rồi OK : --a có nghĩa là mình trừ a ra trước rồi mới in */
console.log(--x); // output : 0

/** Rồi OK : a-- có nghĩa là mình in a ra trước rồi mới tăng lên */
console.log(x--); // output : 1

/**-----------------------------------------------------VÍ DỤ 2----------------------------------------------------------------- */
// Example : a++ + --b + --b + a-- - ++a 
var a = 5;
var b = 6;
var result = a++ + --b + --b + a-- - ++a;
/**          ---------   ---------   ---- 
 *            cụm 1       cụm 2      cụm 3
 * */
console.log('Result :', result);

/**
 *  Giải:
 *  a = 5 -> a++ = 5 |                   |
 *                   |--> a++ + --b = 10 |
 *  b = 6 -> --b = 5 |                   |  
 *                                       |
 *  Tiếp tục :                           |
 *                                       |
 *  b = 6 -> --b = 5 |                   |==> result = a++ + --b + --b + a-- - ++a = 14
 *                   |-> --b + a-- = 10  |
 *  a = 5 -> a-- = 5 |                   |
 *                                       |
 *  Tiếp tục :                           |  
 *                                       |  
 *  a = 5 -> ++a = 6 |-> ++a = 6         |  
 *  
 * Tưởng tự như trên bạn hãy thử đổi: + <=> *,- <=> / và phân tích như trên nhé
 */