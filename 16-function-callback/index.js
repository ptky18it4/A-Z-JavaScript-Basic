/**
 * function as a parameter (callback)
 * dùng function như một tham số 
 */
// Function: Máy pha cafe
//B1 :


var coffeeMachine = {
    makeCoffee: function(onFinish) { // on..chỉ là một kiểu đặt tên 
        console.log('Making coffee .. ');
        // return 'Bip bip';
        /**
       
         *  Kết quả trả về bạn sẽ thấy có undefine vì sao ? bời vì chúng ta chỉ  console.log('Making coffee .. ')
            ra thôi chứ không return về cái gì cả.
         */
        onFinish();

    }
};


// console.log(coffeeMachine);
// console.log(coffeeMachine.makeCoffee());
// console.log('\n-------------------------------');

var beep = function() {
    console.log('Beep beep ....');
};

var c = coffeeMachine.makeCoffee(beep); //naming convention
console.log(c);