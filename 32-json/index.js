/**
 * JSON Object
 * 1. stringify - Convert an object to a JSON string
 * 2. parse - Convert a JSON strong to an object
 * 
 * Exercise
 * 1. Show all students
 * 2. Create a new student
 * 3. Save & Exit
 * > 1.
 * Save to ./data.json -> fs.readFileSync, JSON.parse
 * > 2.
 * Your name?
 * Your age?
 * Your class?
 * > 3. Out programming
 */

var myDog = { name: 'Milu', age: 1, dead: false };
var myDogString = JSON.stringify(myDog);
console.log(typeof myDog);
console.log(typeof myDogString);
/**
 * Chúng ta chú ý dấu ngoặc kép ở mỗi key -> string
 * Khi là string thì ta có thể ghi vào file được
 */
/**CONVERT */

var myCatString = '{"name": "Tom", "age": 2, "dead": true}';
var myCat = JSON.parse(myCatString);
console.log(myCat);