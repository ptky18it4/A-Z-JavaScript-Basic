/**
 * Bạn hãy dành ra 5 - 10 phút để xem lại nội dung bài cũ để củng cố lại kiến thức, tọa đà cho bài học ngày hôm nay nhé
 * Rồi OK, hãy cùng nhau bước vào bài học thứ 9 trong chuỗi các bài giảng A-Z JavaScript Basic 
 * Ngày hôm nay ta sẽ học bài : 
 *                                  - Assignment operators -
 *                                      -- các phép gán --
 * Trong các ngôn ngữ lập trình thì có các phép gán sau đây :  =, += , -=, *=, /=
 * Thì trong JavaScript cũng tương tự như vậy, cũng có các toán tử gán như trên, và bài hôm nay mình sẽ giải thích cụ thể
 * cho các bạn về những phép gán này để code của chúng ta trở nên gọn gàng hơn, pro hơn và mọi người nể hơn. :)) 
 * 
 * "chém gió vậy thôi chứ chả ông nào nể bạn đâu, muốn người ta nể thì hãy học đi nghen"
 * 
 * Rồi bắt đầu thôi.
 */
console.log('\nPhép gán = ');

var a = 5;
var result = 0;
console.log(result = a); //--> output : result = 5
console.log('\nPhép gán += ');

var b = 5;
var result_b = 0;
console.log(result_b += b); //--> output : result = result + b = 0 + 5 = 5
console.log('\nPhép gán -= ');

var c = 5;
var result_c = 0;
console.log(result_c -= c); //--> output : result = result - c = 0 - 5 = -5
console.log('\nPhép gán *= ');

var d = 5;
var result_d = 0;
console.log(result_d *= d); //--> output : result = result * d = 0 * 5 = 0
console.log('\nPhép gán /= ');

var e = 5;
var result_e = 0;
console.log(result_e /= e); //--> output : result = result / e = 0 / 5 = 0

/**
 * Rồi: OK
 * Example : var a = 5; var b = 7; var c = 9; var d = 10
 * Dựa vào lời giải bài tập trước để trình bài cho lời giải của bài này :
 * Yêu cầu tính : result = ++a * a++ / b-- + c++ + ++d.
 * - tiếp theo  : result_1 += ( result + ++a / b-- )
 * - tiếp theo  : result_2 *= result_1 + ++result
 * - xuất ra màn hình kết quả của result_2.
 */