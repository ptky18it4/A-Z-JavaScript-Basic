/**
 * Hôm nay chúng ta sẽ tìm hiểu về bài 2 khái niệm mà có một số bạn đăng thắc mắc nhé :
 *  sync ? Synchoronous
 *  async ? Asynchoronous
 * 
 */
var fs = require('fs');

// console.log('Start');
var song1 = fs.readFileSync('song1.txt', { encoding: 'utf8' });
// console.log(song1);
var song2 = fs.readFileSync('song2.txt', { encoding: 'utf8' });
// console.log(song2);
// console.log('End');

/**
 * 
 * Make coffee : 
 *  time : 5 minute,
 *  job  : giặc đồ, pha coffe,
 *  Để hiểu đơn giản hơn thì chúng ta hay tưởng như sau :
 *  hôm nay chúng ta có 2 việc : pha coffe và giặc đồ
 *  -> thì có 2 cách sau đây : pha coffe trước rồi giặc đồ ( job 1)
 *  ->                       :  giặc đồ trước rồi pha coffe ( job 2)
 *  -> thì rõ ràng là job sẽ tối ưu hóa thời gian hơn đúng không. 
 * Sync
 *  ----------------------|--------------------------------|----------> (t) / Sync
 * giặc đồ            pha coffee                           Done 
 *                    giặc đồ done
 * 
 * Async
 *  *  ----------------------|--------------------------------|----------> (t) / Async
 * pha coffe            pha coffee done                     Done 
 *                      giặc đồ
 **/

console.log('Start');
fs.readFile('song1.txt', { encoding: 'utf8' }, function(err, song1) {
    console.log(song1);
})
console.log('End');