/**
 * Các method của 1 array
 * -a.concat(b)  : nối 2 mảng
 * -a.push(b)    : thêm phần tử mới vào trong mảng , ở vị trí cuối cùng
 * -a.pop(b)     : lấy phần tử cuối cùng của mảng- thay đổi mảng
 * -a.shift(b)   : đẩy phần tử đầu tiên ra khỏi mảng
 * -a.unshift(b) : nhét phần tử vào vị trí đầu tiên của mảng
 * Tự đọc lên MDN (Mozilla Developers Network), javascript.infor : https://javascript.info/array-methods
 * -a.slice
 * -a.splice
 * Bài sau: find, filter, sort, map, reduce, etc.
 *----------------------------------------------------ARRAY-METHODS---------------------------------------------------------------
 * OK vào bài nhé!
 * 
 */
//A.CONCAT(B)
var a_concat = [1, 2, 3];
var b_concat = [5, 6, 7];
console.log(a_concat.concat(b_concat));
console.log('\n-----------------------------------------------------------------------');
/**
 * output: [ 1, 2, 3, 5, 6, 7 ]
 * 2 mảng a và b không thay đổi, mà nó tạo ra 1 mảng mới và nối thằng b vào a
 * -Muốn lưu lại mảng đó thì gán nó cho 1 biến: var c = console.log(a.concat(b))
 */
//A.PUSH(B)
var a_push = [1, 2, 3];
var b_push = [4];
console.log(a_push.push(b_push));
console.log('\n-----------------------------------------------------------------------');
/**
 * Kết quả trả về là 4 , bởi vì sao , vì giá trị trả về không phải là 1 cái mảng mới
 * mà nó trả về độ dài mới của mảng a.
 * doc : Appends new elements to an array, and returns the new length of the array.
 */
var a_pop = [1, 2, 3];
console.log(a_pop.pop());
console.log(a_pop);
console.log('\n-----------------------------------------------------------------------');
/**
 * doc : Removes the last element from an array and returns it.
 * Hiểu : trả về giá trị bị đây ra ( phẩn tử cuối cùng của mảng ) và trả về mảng mới chứa các phần tử còn lại
 */
var a_shift = [1, 2, 3];
console.log('a_shift : ', a_shift.shift());
console.log('Mảng mới : ', a_shift);
console.log('\n-----------------------------------------------------------------------');
/**
 * doc : Removes the first element from an array and returns it.
 * Hiểu : đầy thằng đầu tiên của mảng ra , trả về mảng mới chứa các phần tử còn lại
 */
var a_unshift = [1, 2, 3];
var b_unshift = -1;
console.log('a_shift : ', a_unshift.unshift(b_unshift));
console.log('Mảng mới : ', a_unshift);
console.log('\n-----------------------------------------------------------------------');
/**
 * doc : Removes the first element from an array and returns it.
 * Hiểu : đầy thằng đầu tiên của mảng ra , trả về mảng mới với phần tử thêm mới đúng đầu tiên , sau đó là các phần tử cũ
 */