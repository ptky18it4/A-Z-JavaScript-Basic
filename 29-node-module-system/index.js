// console.log('Welcome to js basic');

var Mouse = require('./mouse');
var Cat = require('./cat');

// Create new mouse

var mickey = new Mouse('black');
var jerry = new Mouse('yellow');
console.log(mickey);
console.log(jerry);

// Cat eat mouse

var tom = new Cat();

tom.eat(mickey)
tom.eat(jerry)

console.log(tom);