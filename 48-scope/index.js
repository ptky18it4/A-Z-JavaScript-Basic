/**
 * Hello !
 * Hôm nay sẽ học về phạm vi hoạt động của biến trong javascript nhé !
 * 1. Global scope
 * 2. Local scope
 * when a function is excuted -> create a new scope
 * - Không nên để phạm vị hoạt động của một biến quá lớn
 * - tránh thay đổi đầu vào.
 */
var a = 1;

function random() {
    var b = Math.random();
    console.log(b);
}
random();