// /**
//  * Bài hôm nay sẽ giới thiệu về Promise
//  * Thì để bắt đầu học về Promise thì ta cần phải cài module : 
//  * Syntax install : npm i --save promise-fs
//  * Các bước cài :
//  * B1 :  npm init
//  * B2 :  dùng chiêu nhất dương chỉ -> Enter -> xong !
//  * Note : nhớ nhất Enter liên tục nhé !
//  *   
//  */
// var fs = require('promise-fs');


// fs.readFile('song1.txt', { encoding: 'utf8' })
//     .then(function(song1) {
//         console.log(song1);
//     })
//     .catch(function(error) {
//         console.log(error);
//     })

// /** Optimeze : có thể code như sau để code đẹp hơn nhé */
// function onDone(song) {
//     console.log(song);
// }

// function onError(error) {
//     console.log(error);
// }
// fs.readFile('song1.txt', { encoding: 'utf8' })
//     .then(onDone)
//     .catch(onError)
//     /** Optimeze : có thể code như sau để code đẹp hơn nhé */
// function onDone(song) {
//     console.log(song);
// }

// function onError(error) {
//     console.log(error);
// }

// function readFile(path) {
//     return fs.readFile(path, { encoding: 'utf8' });
// }
// fs.readFile('song1.txt', { encoding: 'utf8' })
// readFile('song1.txt')
//     .then(onDone)
//     .then(function() {
//         return readFile('song2.txt');
//     })
//     .then(onDone)
//     .catch(onError)

/**
 * À chú ý xíu nhé :
 * như bạn thấy ở trên có đặt tên là onDone và onError thì từ on phía
 * trước cũng không có ý nghĩa gì cao xa cả, chẳng qua là nó là một quy
 * tắc đặt tên trong khi sử dụng Promise thôi à. 
 * Thôi thì cứ bắt chước các bậc cao nhân để  dễ  sống :))
 */
// Begin
var fs = require('fs');

function readFilePromise(path) {
    return new Promise(function(resolve, reject) {
        fs.readFile(path, { encoding: 'utf8' }, function(err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

readFilePromise('song1.txt')
    .then(function(song1) {
        console.log(song1);
    })
    .catch(function(error) {
        console.log(error);
    })
    // End