/**
 * Hello, Hôm nay sẽ học về 1 module tiếp theo cũng khá
 * là quan trong đó chính là "co" ????
 *          cò , cổ,  có, co , cọ, ????
 * Làm sao để đơn giản hóa lập trình không đồng bộ Async trong javascript
 * Thì bây giờ mình sẽ ghé hầm rựu của một cao nhân tên gì (:)) tj
 * chú ý : đây là một module của một bên thứ 3 chứ không phải của hệ thống https://nodejs.org/en/docs/
 * Thì các bước cài cũng đơn giản thôi:
 * Syntax : 
 * B1 : npm init
 * B2 : Nhất dương chỉ Enter ( Enter cho đến khi ....tự hiểu )
 * B3 : npm install co --save
 * link : https://github.com/tj/co
 *  */
var fs = require('fs');
var co = require('co');

function readFilePromise(path) {
    return new Promise(function(resolve, reject) {
        fs.readFile(path, { encoding: 'utf8' }, function(err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

co(function*() { // generator function để xem cách dùng của nó, hiểu tại sao có dấu  *

    /**Kiểu cổ điển - yield any promise */

    // var song1 = yield readFilePromise('./song1.txt');
    // var song2 = yield readFilePromise('./song2.txt');
    // var song3 = yield readFilePromise('./song3.txt');


    /** Ngoài cách code như trên thì có thể  dùng yield with array[ các readFilePromise] như sau */

    var values = yield [
        readFilePromise('./song1.txt'),
        readFilePromise('./song2.txt'),
        readFilePromise('./song3.txt')
    ];
    //C1 
    // return [song1, song2, song3];
    //C2
    return values;

}).then(function(values) {
    console.log(values);
}).catch(function(error) {
    console.log(error);
})

console.log('\n');
/**Cách dùng thứ 2 */
/**
 * Thằng co.wrap không trả về  Promise và nó trả về một hàm ( function ).
 * Vậy thì từ hàm (function) ->  Promise : thì làm như thế  nào ?
 */
var readFile = co.wrap(function*(files) {
    //[String] -> [Promise]
    var values = yield files.map(function(file) {
        return readFilePromise(file);
    });
    return values;
});

readFile(['song1.txt', 'song2.txt', 'song3.txt'])
    .then(function(values) {
        console.log(values);
    });