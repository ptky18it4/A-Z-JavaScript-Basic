/**
 * OK, thì ở bài cũ cũng ta đã học về : map, filter, find. Đây làm một số method khá là hay trong javascript.
 * Bạn hãy dành thời gian tầm 5-10 phút/ mỗi bài  đề  cover lại kiến thức nhé ! 
 * Rồi Ok, bắt đầu bài mới thôi:
 * reduce : giảm : giảm về số lượng
 * hoạt động: 
 * ứng dụng :
 * -------------------------------------------------------------------------------------------------------------------------------
 *                                          [x1     ,   x2            ,x3           ,x4]
 *                                          ___________________________________________
 *                                            \        /              /              /
 *                                             \      /              /              /
 *                                             ________             /              /
 *                                             \      /            /              /
 *                                              \    /            /              /
 *                                               |  |            /              /
 * (result <- reduce function(params_1,params_2)  y1            /              /
 *                                                   \         /              /
 *                                                    \       /              /
 *                                                     ________             /
 *                                                     \      /            /
 *                                                      \    /            /
 *                                                       |  |            /
 *          (result <- reduce function(params_1,params_2) y2            /
 *                                                          \          /                                    
 *                                                           \        /
 *                                                            _________
 *                                                            \       /
 *                                                             \     /
 *                                                              |   |
 *                                                                y3 (result <- reduce function(params_1,params_2)
 * 
 * 
 * 
 * 
 
 

 */
var numbers = [1, 2, 3, 4];

var result = numbers.reduce(function(item_1, item_2) {
    console.log(' _ ', item_1, ' + ', item_2, ' = ', result = item_1 + item_2);
    return item_2 = result;


});
console.log('Result : ', result);