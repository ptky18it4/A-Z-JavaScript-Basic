/**
 * Math object in JavaScript
 * Math.PI
 * Math.ceil(number) : ceilling : làm tròn lên, số thập phân : 9.3 -> 10, 9.7 -> 10
 * Math.floor(number) : flooring : làm tròn xuống, số thập phân : 9.7 -> 9, 9.4 -> 9
 * Math.round(number)
 * Math.max(x1,x2,...,xn)
 * Math.min(x1,x2,...,xn)
 * Math.random()
 * Math. ...
 * Google keyword : Mozilla Math object, Javasctip.infor
 */
function discArea(r) {
    return a = r * r * Math.PI;
}
var s = discArea(5);
console.log(s);
console.log('Value of PI : ', Math.PI);
console.log('\n-----------------------------------------------');
var x_ceil = 9.1; // output : 10
console.log(Math.ceil(x_ceil));
console.log('\n-----------------------------------------------');
var x_floor = 9.9; // output : 9
console.log(Math.floor(x_floor));
console.log('\n-----------------------------------------------');
console.log(Math.max(1, 7, 4, 9, 0)); //outout : [0, 1, 4, 7, 9]
console.log(Math.min(1, 7, 4, 9, 0)); //outout : [9, 7, 4, 1, 0]
console.log('\n-----------------------------------------------');
console.log('Random : ', Math.random()); // output : return value from 0-1
console.log('\n-----------------------------------------------');

function tossACoin() {
    var random = Math.random();
    console.log(random);
    return random > 0.5;
}
console.log('Mặt sấp : ', tossACoin());