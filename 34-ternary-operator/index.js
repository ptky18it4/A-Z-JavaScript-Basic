/**
 * ternary operator
 * 
 * Systax : giống toán tử 3 ngôi đó mà ;
 * 
 * condition ? expression when true : expression when false
 */
/** TUNG ĐỒNG SU */
function tossCoin() {
    // show random values
    var value = Math.random();
    var result = (value < 0.5) ? 'Mặt sấp' : 'Mặt ngửa';

    // if (value < 0.5) {
    //     result = 'Mặt sấp';
    // } else {
    //     result = 'Mặt ngửa';
    // }
    console.log(result);
}
tossCoin();