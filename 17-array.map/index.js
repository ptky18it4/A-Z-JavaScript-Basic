/**
 * Map : hay hay còn gọi là ánh xạ: biến đổi 1 tập hợp những phần tử thành 1 tập hợp những phần tử mới theo một quy tắc nào đó.
 * Quy tắc đó được định nghĩa bởi 1 function 
 * 
 *                     [ 1      ,2      ,3      ,4 ]
 *                       |       |       |       |
 *                       --------------------------
 *                       |       |       |       |
 *                       |       |  MAP  |       |
 *                       |       |y = x^2|       |
 *                       -------------------------- 
 *                       |       |       |       |
 *                       |       |       |       |
 *                     [ 1      ,4      ,9      ,16 ]
 *          
 * 
 *              arr.map(function(item){
 *                  //transform function 
 *                  return newValue;
 *              })
 */
var numbers = [1, 2, 3, 4, 5];
var squareNumbers = numbers.map(function(x) {
    return x * x;
});
console.log(squareNumbers);
/**
 * Bài tập :
 * cho mảng dưới đây: dùng map method để biến đôi rectangles thành 1  array mới gồm có diện tích của các hình  trên 
 */
console.log('\n----------------------------------------------------------------');
var restangles = [
    { width: 15, height: 3 },
    { width: 10, height: 2 },
    { width: 8, height: 4 }
];
var w, h, squareRestangles;
//Use for-of
for (var restangle of restangles) {
    // console.log(restangle.width * restangle.height);
}
//Use array.map
var rectangles = [
    { width: 10, height: 5 },
    { width: 11, height: 3 },
    { width: 7, height: 6 }
];

var rectanglesArea = rectangles.map(function(item) {
    return item.width * item.height;
})
console.log('Dien tich : ', rectanglesArea);
/**
 * Mỗi item = { width : ..., height : ....}
 * Do đó : nếu muốn lấy giá trị của key ( width, height) thì ta dùng item.key
 */