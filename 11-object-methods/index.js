/**
 * Hey - xin chào bạn. Đừng quên dành ra khoảng thời gian từ 5-10 phút cover bài cũ (bài 5-object) - tọa tiền đề cho bài mới nhé
 * Bài hôm nay của chúng ta đó chính là :
 * ------------------------------------------------------------------------------------------------------------
 *                               --OBJECT METHODS / CÁC PHƯƠNG THỨC CỦA OBJECT--
 * ------------------------------------------------------------------------------------------------------------
 * 
 */
// var human = {
//     name: 'Kenneth',
//     age: 19
// };
/**
 * Như bạn thấy ở trên ! bạn chỉ mới khai báo ra 1 human với các thuộc tính cơ bản, chưa có hành động.
 * Tuy nhiên, trong lập trình thì phải tạo ra cái gì đó có thể họat động được, chứ k thì object tạo ra cũng vô dụng.
 * Vậy thì bây giờ mình sẽ cần thêm vào một hành động nhỏ cho nó ( object human)
 */
var human = {
    name: 'Kenneth',
    age: 19,
    weight: 10,
    talk: function() {
        /**
         * Như bạn thấy ở đây thì function này khồn có tên.
         * Trong lập trình mọi người hay gọi là : anonymous function
         * Để ý rằng: 
         * khi 1 function() đứng riêng một mình thì --> gọi nó là function
         * khi 1 function() được gắng như là một giá trị cả thuộc tính trong 1 object thì gọi nó là method
         * 
         */
        console.log('Trong trường hợp này nó sẽ return ra undefind')
    },
    eat: function(bone) {
        this.weight = this.weight + bone.weight;
        return this;
    }
};
var bone = { weight: 20 };
console.log('Before eating : ', human.age);

human.eat(bone);
console.log('After eating : ', human.weight);