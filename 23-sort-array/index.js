/**
 * filter, find, map, reduce
 * array.sort
 * syntax : nameArray.sort(function(a,b) {}) ->return a sorted array
 *                              ^sort function
 * if sort function
 * 1. returns a value < 0
 * a will come before b
 * 2. returns a value > 0
 * a will come after b
 * 3. returns 0
 * a and b will stay unchanged
 */
var numbers = [2, 9, 3, 4, 1];
// [1, 2, 3, 4, 9] // ascending order
var ascendingNumber = numbers.sort(function(a, b) {
    return a - b;
});
console.log(ascendingNumber);

// [9, 4, 3, 2, 1] // descending order
var descendingNumbers = numbers.sort(function(a, b) {
    // assume: a = 3, b = 4
    // expect: a comes after b
    return b - a;
});
console.log(descendingNumbers);

// Example
var employees = [
    { name: 'Tony', age: 18 },
    { name: 'Ken', age: 9 },
    { name: 'Pin', age: 28 },
    { name: 'Jack', age: 8 }
];
console.log(employees);
console.log('\n---------------------------------');
var sortedEmployees = employees.sort(function(a, b) {
    // a = employees[1], b = employees[2]
    // expect : a comes after b
    return a.age - b.age; // > 0
});
console.log(sortedEmployees);