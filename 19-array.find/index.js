/**
 * find : trong tiếng anh : tìm kiếm
 * Hiểu đơn giản đó là : find dùng để tìm 1 phần tử nào đó giống với phần tử  mà người dùng nhập vào 
 *-----------------------------------------------------------------------------------------------------------------------------
 *                 [x1      ,x2      ,x3      ,x4]                             [x1      ,x2      ,x3      ,x4]
 *                ________________________________                             ________________________________  
 *               |                                |                           |                                |    
 *                \                              /                             \                              /
 *                 \                            /                               \                            / 
 *                  \         Điều kiện        /                                 \                          /
 *                   \                        /                                   \                        / 
 *                    \    filter function   /                                     \     find function    /
 *                     \                    /                                       \                    /
 *                      \                  /                                         \                  /
 *                       \                /                                           \                /
 *                        \              /                                             \              /
 *                         \            /                                               \            /
 *                          \          /                                                 \          /
 *                           \        /                                                   \        /
 *                   _____________________________                              ______________________________
 *                            |      |                                                     |       |
 *                          [x1     ,x4]                                                      x3
 * Rồi OK, hoàn thành xong khóa này. Trình vẽ của mình sẽ tăng cao   |  find: khi nào find function trả về giá trị true thì nó sẽ dừng lại ngay
 *     
 */
var numbers = [1, 2, 3, 4];
var evenNumbers = numbers.find(function(item) {
    console.log(item);
    return item % 2 === 0;
});
console.log('First sult :', evenNumbers);