var products = [
    { name: 'Casio STANDARD AE-1200WHD-1AV', unitPrice: '1.120.000', urlImage: 'https://www.g-shock.com.vn/wp-content/uploads/2018/05/ae-1200whd-1av.png', category: 'Casio' },
    { name: 'Casio G-SHOCK GA-120-1A', unitPrice: '3.190.000', urlImage: 'https://www.g-shock.com.vn/wp-content/uploads/2017/08/ga-120-1a.png', category: 'Casio' },
    { name: 'Casio G-SHOCK GA-100-1A4', unitPrice: '1.120.000', urlImage: 'https://www.g-shock.com.vn/wp-content/uploads/2017/08/ga-100-1a4.png', category: 'Casio' },
    { name: 'Casio STANDARD AE-1200WHD-1AV', unitPrice: '4.190.000', urlImage: 'https://www.g-shock.com.vn/wp-content/uploads/2017/08/ga-100-1a1.png', category: 'Casio' },
    { name: 'Casio STANDARD AE-1200WHD-12AV', unitPrice: '1.120.000', urlImage: 'https://www.g-shock.com.vn/wp-content/uploads/2017/08/ga-100-1a1.png', category: 'Victoria' }
];

/* 2 biến dưới đây dùng để tham chiếu đến 2 id ở tương ứng ở phần html
 : cái này là jquery, từ từ học rồi biết , hí hí */
var productlist = $('#productlist');
var categoryFilter = $('#categoryFilter');

/* Tạo ra một cái gọi là render( tham số 1, tham số 2), mục đích để làm chi ?
 	+ Thứ nhất : để chuyển đổi các dữ liệu trong mảng products sang định dạng show ra màn hinfhc ho người dùng xem đó các cháu 
*/
render(productlist, products);
/* Ta thấy, có phần chọn loại hàng đúng không ?
	Để làm được thì cần phải tạo ra 1 cái gọi là
  categoryFilter để nó lọc ra các sản phẩm thuộc loại mà người dùng chọn. Ok here we go
 */
categoryFilter.on('change', function() {
    var selectedCategory = this.value //choose Casio or Victoria
        //write code here
    var filteredCategory = products.filter(function(product) {
        return product.category === selectedCategory;
    });
    render(productlist, filteredCategory)
});

function render(container, items) {
    // container === ul
    // item === lists user
    //write code here
    var htmlItems = items.map(function(item) {
        return '<div class="card"' + '<li style="padding: 16px;" class="list-group-item">' + item.name + '</li>' + '<img style="width: 100px; " src="' + item.urlImage + '">' + '<li class="list-group-item">' + item.unitPrice + '</li>' + '<div>';
    });
    //html = 
    // ['<li class="list-group-item">Tom</li>', <li class="list-group-item"> ...']
    var html = htmlItems.join('');
    //html = <li class="list-group-item">Tom</li>, <li class="list-group-item"> ...
    container.html(html);
}


/**
 * link online: https://jsfiddle.net/TrungKy/y4eabk9j/
 */