/**
 * OK, xin chào bạn, để tiếp tục bài hôm nay thì hãy dành ra 5-10 phút để xme lại bài cũ nhé !
 * -Tiếp tục bài hôm nay mình sẽ gửi đến bạn đường link để học về  các kiểu dữ liệu trong JavaScript
 * -Nguồn tài liệu : link : https://javascript.info/types
 * -Một số nội dung chính cần chú ý
 * -Primitives
 *      -Number
 *      -String
 *      -Boolean
 * -Special type
 *      -undefind : 
 *      -null : 
 * -Reference types
 *      -Object
 *      -Array
 *      
 */
//Number
var a = 1;
var b = 2;
var c = a + b;
console.log('Tổng : ', c);

//String : trong bài này sẽ có nhắc đến 1 kỹ thuât : escape a character  (\)
var str = 'Hello world';
var strNick = 'My nickname is : Kenneth';
console.log(strNick);
var strName = 'Hello, I\'m Kenneht'; // chú ý dâu  (\)

//Boolean ( true / false)

// Giải thích về  : undedind
var bienUndefind;
console.log(bienUndefind); //--> output : undefind

// Giải thích về  : null
var bienNull = null;
console.log(bienNull); //--> output : null


/**||||||||||||||||||||||||||||||||||||||||||||||||||- BÀI 5 : ARRAY ( MẢNG) / OBJECT --|||||||||||||||||||||||||||||||||||||||||||||||*/