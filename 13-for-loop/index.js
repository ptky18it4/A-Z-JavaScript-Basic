/**
 * Bạn hãy dành thời gian tầm 5 - 10 phút để ôn lại nội dung bài cũ / hoàn thành Examples của bài hôm trước nhé
 * Rồi OK. Thì hôm nay chúng ta sẽ học bài : 
 * -----------------------------------------------------------------------------------------------------------------------------
 *                                               --FOR LOOP / VÒNG LẶP FOR--
 * ----------------------------------------------------------------------------------------------------------------------------- 
 *                                               --| for, for-of(học ở bài 14)), ....|--
 *
 * Ở bài này mình sẽ không code nhiều, mà chỉ gửi đến bạn đường dẫn dể các bạn vào đọc tài liệu thôi! 
 * Nguồn : https://javascript.info/while-for
 * 
 * Syntax :
 *          for(init; condition; final-expression){
 *              statement;
 *          }
 * keyword : for
 * Hiểu đơn giản: lặp đi lặp lại một việc gì đó với những điều kiện cho đến khi kết thúc vòng lặp (thực hiện xong điều kiện cuối cùng)
 * Thứ tự thực hiện: 
 * 1- init
 * 2- condition (thường trả về giá trị true/ false)
 *      - true : execute -> statement
 *      - false: exit 
 * 3- final-expression 
 */
//Example : hiển thị 0-10
// cách củ chuối 
console.log(0)
console.log(1)
console.log(2)
console.log(3)
console.log(4)
console.log('Mỏi tay rồi nha');
console.log('\n-------------------------------------------------------');
//Cách pro hơn tí
for (var i = 0; i <= 10; i++) {
    console.log(i);
}
/**
 * 1. i = 0
 * 2. 0 <= 10 -> true -> console.log(i);
 * 3. i++
 * 4. i(1) = i(0) + 1 = 0 + 1 = 1 <= 10 -> true -> console.log(i)
 * 5. ...
 * final. i = i(10) + 1 = 11 >= 10 -> false -> exit
 * 
 */
console.log('\n-------------------------------------------------------');
//Example : pro hơn tí : hiển thị danh sách nhân viên
var employees = [
    { name: 'Kenneth', age: 11 },
    { name: 'peter', age: 12 },
    { name: 'Jack', age: 14 }
];
for (var i = 0; i < employees.length; i++) {
    console.log('Tên nhân viên ', i, ':', employees[i].name);
}