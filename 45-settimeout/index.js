/**
 * Hello !. Hôm nay sẽ học về setTimeout nhé!
 * setTimeout(fn, ms); //milisecond
 * Nhiệm vụ : đặt ra 1 khoảng thời gian. chạy hết thời gian thì nó sẽ kết thúc.
 * Dễ hiểu hơn đó là  : setTimeout(run(), 1000);
 * output : sau 1000mls thì run() mới thực hiện)
 */
console.log('Start');
setTimeout(function done() {
    console.log('Finish');
}, 1000);
console.log('Done');

//
var done = function() {
    console.log('Finish');
}
console.log('Start');
var timeoutId = setTimeout(done, 1000);
console.log('Done');
clearTimeout(timeoutId);